.PHONY: dont_run_as_root clean update roles discover ssh_key local_facts gather_facts spreadsheet hosts

cmdb: dont_run_as_root clean update roles discover ssh_key local_facts gather_facts spreadsheet hosts

dont_run_as_root:
	test "$(id -un)" != "root"

clean:
	rm -rf ./roles ./input/* ./artifacts/hosts/* ./artifacts/inventory/* ./tmp/*

update:
	git pull --ff-only

roles:
	rm -rf ./roles
	ansible-galaxy install -r requirements.yml -p ./roles
	
discover:
	bin/discover > ./artifacts/hosts/discovered

ssh_key:
	ifeq ($(LOGNAME),prdansible)
		echo 'Running as prdansible. Skipping SSH key distribution'
	else
		echo "ansible-playbook -k -i ./artifacts/hosts/discovered"
	endif

local_facts:
	ansible-playbook -i ./artifacts/hosts/discovered

gather_facts:
	ansible all -i ./artifacts/hosts/discovered -m setup --tree ./input

spreadsheet:

hosts:
